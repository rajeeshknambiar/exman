/*
 *      exmanwindow.h
 *      
 *      Copyright 2009 Rajeesh K Nambiar <rajeeshknambiar@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#ifndef EXMANWINDOW_H
#define EXMANWINDOW_H

#include <QtGui/QWidget>
#include <QStatusBar>
#include "exmanstorage.h"

namespace Ui
{
	class ExManWindow;
}

class ExManWindow:public QWidget
{
 Q_OBJECT public:
	 ExManWindow(QWidget * parent = 0);
	~ExManWindow();

 protected:
	void closeEvent(QCloseEvent * event);

 private:
	Ui::ExManWindow * ui;
	QString		  fileName;
	bool		  Modified;
	ExManStorage	* accountingInfo;
	ExManEntity	* currentEntry;
	QClipboard	* clipBoard;
	//QStatusBar	* statusBar;

	void		renderNewEntries(ExManEntity & entity);
	void		deleteExistingRows();
	void		addOneEntryAt(int row);
	void		highlightCalendarDate(QDate date, bool set);
	void		highlightFormula(int row, bool set);
	inline bool	isHighLightedFormula(int row);
	inline float	getTotalAmount();
	inline void	setTotalAmount(float newTotal);
	inline float	getMonthlyAmount();
	inline void	setMonthlyAmount(float newTotal);
	inline float	getExcludedAmount();
	inline void	setExcludedAmount(float newTotal);
	inline bool	isCheckedExcludeEntry(int row);
	inline void	initializeMonthlyAmount(int year, int month);
	void		addToTotalAmounts(float newTotal);
	inline		QString getAccountingEntry(int row, int column);
	void		addCustomActions();

private slots:
	void		on_calendarWidget_currentPageChanged(int year, int month);
	void		dateChangedAction(QDate date);
	void		on_addButton_clicked();
	void		on_deleteButton_clicked();
	void		on_accountingTable_cellChanged(int row, int column);
	void		handle_actionCopy();
	void		handle_actionPaste();
};

#endif				// EXMANWINDOW_H
