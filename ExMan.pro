# -------------------------------------------------
# Project created by QtCreator 2009-06-14T19:49:10
# -------------------------------------------------
TARGET = ExMan
TEMPLATE = app
SOURCES += main.cpp \
    exmanwindow.cpp \
    exmanstorage.cpp \
    Calculator.cpp
HEADERS += exmanwindow.h \
    exmanstorage.h \
    Calculator.h \
    MessageException.h
FORMS += exmanwindow.ui
