/*
 *      exmanwindow.cpp
 *      
 *      Copyright 2009 Rajeesh K Nambiar <rajeeshknambiar@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "exmanwindow.h"
#include "ui_exmanwindow.h"
#include "Calculator.h"

#include <QMessageBox>
#include <QCloseEvent>
#include <QDesktopServices>
#include <QTextCharFormat>
#include <QCheckBox>
#include <QClipboard>

#define ITEMS_COL	0
//#define AMOUNT_COL	1
#define FORMULA_COL	1
#define EXCLUDE_COL	2

#define THE_DATA_FILE   (QDesktopServices::storageLocation(QDesktopServices::DataLocation) \
			+ "ExManFile.txt")

ExManWindow::ExManWindow(QWidget * parent):QWidget(parent),
ui(new Ui::ExManWindow)
{
	fileName = THE_DATA_FILE;
	//As of version 0.5, we have changed the file location from user's
	//home($HOME) to user's data location to conform to freedesktop
	//XDG guidelines. In Linux, for e.g, this is in $HOME/.local/share/data
	//Let's migrate the file from old to new location.
	if (! QFile::exists(fileName) ) {	//First Run ;-)
		QString oldFile = QDesktopServices::storageLocation(QDesktopServices::HomeLocation) +
				  "/.ExManFile.txt";
		if (QFile::exists(oldFile))	//If present, copy it and delete
			QFile::copy(oldFile, fileName) && QFile::remove(oldFile);
	}

	Modified = false;
	accountingInfo = new ExManStorage(fileName);
	QDate today = QDate::currentDate();
	currentEntry = new ExManEntity(accountingInfo->readData(today));

	clipBoard = QApplication::clipboard();

	ui->setupUi(this);

	// Set the "Items" column width to fit well in the table
	int tableWidth = ui->accountingTable->width();
	ui->accountingTable->setColumnWidth(ITEMS_COL, tableWidth * 0.45);
	ui->accountingTable->setColumnWidth(EXCLUDE_COL, tableWidth * 0.08);

	//dateChangedAction(QDate::currentDate());
	//Setup the initial monthly total
	initializeMonthlyAmount(today.year(), today.month());
	renderNewEntries(*currentEntry);
	//ui->accountingTable->addAction(ui->actionDelete);

	//Add custom actions
	addCustomActions();
}

ExManWindow::~ExManWindow()
{
	delete ui;
	if (accountingInfo)
		delete accountingInfo;
	delete currentEntry;
	//delete statusBar;
}

void ExManWindow::closeEvent(QCloseEvent * event)
{
	if (Modified) {
		accountingInfo->writeData(*currentEntry);
		accountingInfo->storeBackend();
	}
	event->accept();
}

void ExManWindow::addCustomActions()
{
	//Action for 'Cut (Ctrl+X)' is already mapped to "deleteButton"

	//Add action to enable 'Copy' on Ctrl+C
	//This is enabled by default, but copies only the first selected cell
	QAction *actionCopy = new QAction("Copy",this);
	actionCopy->setShortcut(QKeySequence::Copy);
	this->addAction(actionCopy);
	connect(actionCopy,SIGNAL(triggered()),
		this, SLOT(handle_actionCopy()));

	//Add action to enable 'Paste' on Ctrl+V
	//This is not enabled by default, so we have to do it :-(
	QAction *actionPaste = new QAction("Paste",this);
	actionPaste->setShortcut(QKeySequence::Paste);
	this->addAction(actionPaste);
	connect(actionPaste, SIGNAL(triggered()),
		this, SLOT(handle_actionPaste()));
}

/* Below get/set functions are Inline ;-) */
float ExManWindow::getTotalAmount()
{
	return ui->totalAmount->text().toFloat();
}

void ExManWindow::setTotalAmount(float newTotal)
{
	//ui->totalAmount->setText(QVariant(newTotal).toString());
	QString amt;
	amt.setNum(newTotal, 'f', 2);	//2 decimal places
	ui->totalAmount->setText(amt);
}

float ExManWindow::getMonthlyAmount()
{
	return ui->monthlyAmount->text().toFloat();
}

void ExManWindow::setMonthlyAmount(float newTotal)
{
	//ui->monthlyAmount->setText(QVariant(newTotal).toString());
	QString amt;
	amt.setNum(newTotal, 'f', 2);	//2 decimals
	ui->monthlyAmount->setText(amt);
}

float ExManWindow::getExcludedAmount()
{
	return ui->excludedAmount->text().toFloat();
}

void ExManWindow::setExcludedAmount(float newTotal)
{
	//ui->excludedAmount->setText(QVariant(newTotal).toString());
	QString amt;
	amt.setNum(newTotal, 'f', 2);	//2 decimals
	ui->excludedAmount->setText(amt);
}

void ExManWindow::addToTotalAmounts(float delta)
{
	setTotalAmount(getTotalAmount() + delta);
	setMonthlyAmount(getMonthlyAmount() + delta);
}

QString ExManWindow::getAccountingEntry(int row, int column)
{
	return ui->accountingTable->item(row, column)->text();
}

bool ExManWindow::isCheckedExcludeEntry(int row)
{
	return ui->accountingTable->item(row, EXCLUDE_COL)->checkState();
}

void ExManWindow::initializeMonthlyAmount(int year, int month)
{
	float total = 0.00, exclude = 0.00;
	foreach(QDate presentDate, accountingInfo->getDateList(year, month)) {
		total += accountingInfo->getTotalforDate(presentDate, true);
		exclude += accountingInfo->getTotalforDate(presentDate, false);
		//Highlight the dates for which an account item is present
		highlightCalendarDate(presentDate, true);
	}
	setMonthlyAmount(total);
	setExcludedAmount(exclude);
}

void ExManWindow::highlightCalendarDate(QDate date, bool set)
{
	QTextCharFormat highlight;
	set ? highlight.setBackground(Qt::magenta) :
		highlight.clearBackground();
	//setNoBold.setFontWeight(QFont::Bold);
	//setBold.setFontPointSize(ui->calendarWidget->font().pointSize() + 2);
	//setNoBold.setFontItalic(false);
	ui->calendarWidget->setDateTextFormat(date, highlight);
}

void ExManWindow::highlightFormula(int row, bool set)
{
	//Mark the erroneous formula in RED or not
	set ? ui->accountingTable->item(row, FORMULA_COL)->setForeground(Qt::red) :
	      ui->accountingTable->item(row, FORMULA_COL)->setForeground(Qt::black);
}

bool ExManWindow::isHighLightedFormula(int row)
{
	return ui->accountingTable->item(row,FORMULA_COL)->foreground() == Qt::red;
}

void ExManWindow::on_accountingTable_cellChanged(int row, int column)
{
	// On drag 'n drop, it could happen that the drop target
	// is a nonexisting row. In that case, we need to handle it.
	// If we simply return here, Qt will create a row, but it won't set
	// the eXclude button, as it is done by us. Also, we can't raise
	// on_addButton_clicked either, as it would immediately raise
	// on_accountingTable_cellChanged and then go to infinite loop.
	// To solve this, use the method to add one entry, and use the same
	// in both here and on_addButton_clicked
	if (row >= currentEntry->items.count()) {
		addOneEntryAt(row);
		return;
	}
	if (currentEntry->date.isNull()) {
		currentEntry->date = ui->calendarWidget->selectedDate();
		highlightCalendarDate(currentEntry->date, true);
	}
	ExManItem currentItem = currentEntry->items[row];
	switch (column) {
	case ITEMS_COL:
		{
		QString newTxt = getAccountingEntry(row, column);
		if (currentItem.item != newTxt) {
			currentItem.item = newTxt;
			Modified = true;
		}
		break;
		}
	//case AMOUNT_COL:
	case FORMULA_COL:
		{
		//float newAmount = getAccountingEntry(row, column);
		float newAmount = 0.00;
		float curAmt;
		curAmt = currentItem.formulaError? 0.00 : currentItem.amount();

		ExManItem *tmpItem = new ExManItem;
		tmpItem->formula = getAccountingEntry(row, column);

		if (tmpItem->formula == currentItem.formula) {
			delete tmpItem;
			return;	//No change, formula evaluation is costly :-)
			//Further, this check is required to avoid the rest of
			//the logic on highlightFormula().
		}

		//Evaluate the new formula
		try {
			newAmount  = tmpItem->amount();
		}

		catch (CalculatorException e) {
			//Formula is erroneous!
			newAmount = 0;
			tmpItem->formulaError = true;
			currentEntry->items[row].formula = tmpItem->formula;
			ui->statusBar->showMessage(e.getMessage().c_str());
		}

		Modified = true;
		//Subtract the old amount
		//float curTotal=getTotalAmount() - currentEntry->items[row].amount;
		float delta = newAmount - curAmt;
		currentItem.formula = tmpItem->formula;
		currentItem.formulaError = tmpItem->formulaError;
		delete tmpItem;
		//curTotal += currentEntry->items[row].amount; //Add the new amount
		//setTotalAmount(curTotal);
		addToTotalAmounts(delta);
		if (currentItem.exclude)
			setExcludedAmount(getExcludedAmount() + delta);
		break;
		}
	case EXCLUDE_COL:
		{
		bool isChkd = isCheckedExcludeEntry(row);
		if (currentItem.exclude == isChkd)
			return;
		currentItem.exclude = isChkd;
		float curAmt;
		curAmt = currentItem.formulaError ? 0.00 : currentItem.amount();
		if (currentItem.exclude)
			setExcludedAmount( getExcludedAmount() + curAmt );
		else
			setExcludedAmount( getExcludedAmount() - curAmt );
		Modified = true;
		break;
		}
	}
	if(Modified)
		currentEntry->items[row] = currentItem;

	//If formula entered is erroneous, highlight. Otherwise, clear it
	//NOTE : highLightFormula raises cellChanged again, but when
	//the inner call returns, doesn't go back to the outer call,
	//the inner call is equivalent to return.
	//This is because setForeground internally calls SetData,
	//which triggers cellChanged. But on the second time, since
	//foreground is already set, it won't take any effect and
	//hence doesn't go into infinite loop.
	if (column == FORMULA_COL)
	currentItem.formulaError? highlightFormula(row, true) :
			( ui->statusBar->clearMessage(), highlightFormula(row, false) );
}

/* Gets triggered when a row is deleted	*/
/* Triggered when Ctrl+X is pressed too */
void ExManWindow::on_deleteButton_clicked()
{
	//Copy the data in deleting rows to clipBoard
	handle_actionCopy();

	//Proceed with deleting the rows
	QList <int> deletedRows;	//Keep track of deleted rows
	float subtractTotal = 0.00, subExclude = 0.00;
	int count = 0;		//Whenever a row is removed, rows below are moved up
	//Delete the selected rows
	//Heck, we can't get only the selected rows!
	foreach(QModelIndex index,
		ui->accountingTable->selectionModel()->selectedIndexes())
	    if (!deletedRows.contains(index.row())) {
		//Make sure to adjust the row number as they are automatically
		//moved up on each delete
		int row_to_del = index.row() - count;
		float amt;
		amt = currentEntry->items[row_to_del].formulaError ? 0.00 :
				currentEntry->items[row_to_del].amount();
		subtractTotal -= amt;
		if (currentEntry->items[row_to_del].exclude)
			subExclude -= amt;
		ui->accountingTable->removeRow(row_to_del);
		currentEntry->items.removeAt(row_to_del);
		++count;
		deletedRows.append(index.row());
	}
	deletedRows.clear();
	if (subtractTotal) {
		Modified = true;
		addToTotalAmounts(subtractTotal);//NOTE : subtractTotal is negative
	}
	if (subExclude) {
		Modified = true;
		setExcludedAmount(getExcludedAmount() + subExclude);
	}
	//If all entries are removed for the date, stop highlighting it
	if (ui->accountingTable->rowCount() == 0)
		highlightCalendarDate(currentEntry->date, false);
}

/* Triggered when the 'addButton' is pressed	*/
void ExManWindow::on_addButton_clicked()
{
	int lastRow = ui->accountingTable->rowCount();
	ui->accountingTable->insertRow(lastRow);
	addOneEntryAt(lastRow);
}

/* Triggered when Ctrl+C is pressed	*/
void ExManWindow::handle_actionCopy()
{
	QString buffer, txt;
	foreach(QTableWidgetItem *itm, ui->accountingTable->selectedItems()) {
		if(itm->column() == EXCLUDE_COL) {
			itm->checkState() ? txt = "\tX\n": txt = "\t\n";
			buffer.append(txt);
		}
		else {
			buffer.append("\t");
			buffer.append(itm->text());
		}
	}
	clipBoard->setText(buffer);
}

/* Triggered when Ctrl+V is pressed	*/
void ExManWindow::handle_actionPaste()
{
	QTableWidgetItem *itm;
	QString oneline;
	QStringList tokens;
	int i;

	QString buffer = clipBoard->text();
	QStringList lines = buffer.split(QRegExp("\n"),QString::SkipEmptyParts);
	if ( lines.isEmpty() )
		return;
	do {
		oneline = lines.takeFirst();
		tokens = oneline.split( QRegExp("\t") );
		//First field will always be <tab>, get rid of it
		tokens.removeFirst();
		i = 0;
		foreach (itm, ui->accountingTable->selectedItems()) {
			if (tokens.empty())
				break;
			if (itm->column() == EXCLUDE_COL) {
				(tokens.takeFirst() == "X" )?
					itm->setCheckState(Qt::Checked):
					itm->setCheckState(Qt::Unchecked);
			}
			else
				itm->setText(tokens.takeFirst());
		}
	}while (! lines.isEmpty() );
}

void ExManWindow::addOneEntryAt(int row)
{
	ExManItem oneItem;
	currentEntry->items.append(oneItem);
	//Add a CheckBox for the new item
	QTableWidgetItem *itmEntry	= new QTableWidgetItem("");
	QTableWidgetItem *formulaEntry	= new QTableWidgetItem("");
	QTableWidgetItem *newBox	= new QTableWidgetItem();
	ui->accountingTable->setItem(row, ITEMS_COL, itmEntry);
	ui->accountingTable->setItem(row, FORMULA_COL, formulaEntry);
	ui->accountingTable->setItem(row, EXCLUDE_COL, newBox);
	ui->accountingTable->item(row,
				  EXCLUDE_COL)->setCheckState(Qt::Unchecked);
	//Not required to move label and amount individually, as they are placed in
	//frame, frame->adjustSize() will automatically take care.
	//ui->totalLabel->move(ui->totalLabel->pos().x(), ui->totalLabel->pos().y() + 30);
	//ui->totalAmount->move(ui->totalAmount->pos().x(), ui->totalAmount->pos().y() + 30);
	ui->frame->adjustSize();
	this->adjustSize();	//Adjust window size as well
}

void ExManWindow::dateChangedAction(QDate date)
{
	if (currentEntry->date == date)
		return;

	//Save existing entries to their corresponding Date
	accountingInfo->writeData(*currentEntry);

	//Reset the currentEntry
	deleteExistingRows();

	//Build entries for the new Date chosen
	//currentEntry->date = date;
	*currentEntry = accountingInfo->readData(date);
	if (currentEntry->date == date)
		renderNewEntries(*currentEntry);	//Build Ui for these entries
	else
		setTotalAmount(0.00);
}

void ExManWindow::deleteExistingRows()
{
	//Remove all the rows from tableWidget, and purge currentEntry
	while (!currentEntry->items.isEmpty())
		currentEntry->items.removeFirst();
	while (ui->accountingTable->rowCount())
		ui->accountingTable->removeRow(0);
}

void ExManWindow::renderNewEntries(ExManEntity & entity)
{
	//Build the Ui for Entries for the given date
	int row = 0;
	float newTotal = 0.00;
	foreach(ExManItem itm, entity.items) {
		QTableWidgetItem *newItem = new QTableWidgetItem(itm.item);
		//QTableWidgetItem *newAmount =
		//    new QTableWidgetItem(QVariant(itm.amount).toString());
		QTableWidgetItem *newFormula
				= new QTableWidgetItem(itm.formula);
		//Set the "Exclude" column checked/unchecked
		QTableWidgetItem *newExclude = new QTableWidgetItem();
		if (itm.exclude)
			newExclude->setCheckState(Qt::Checked);
		else
			newExclude->setCheckState(Qt::Unchecked);

		ui->accountingTable->insertRow(row);
		float itmAmt;
		itmAmt = itm.formulaError ? 0.00 : itm.amount();
		newTotal += itmAmt;

		ui->accountingTable->setItem(row, ITEMS_COL, newItem);
		ui->accountingTable->setItem(row, FORMULA_COL, newFormula);
		ui->accountingTable->setItem(row, EXCLUDE_COL, newExclude);
		if (itm.formulaError)
			highlightFormula(row, true);
		++row;
	}
	setTotalAmount(newTotal);
}

void ExManWindow::on_calendarWidget_currentPageChanged(int year, int month)
{
	initializeMonthlyAmount(year, month);
	//Set selected date as first of the month
	//ui->calendarWidget->setSelectedDate(QDate(year,month,1));
	dateChangedAction(QDate(year, month, 1));
}
