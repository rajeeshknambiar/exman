/*
 *      exmanstorage.cpp
 *      
 *      Copyright 2009 Rajeesh K Nambiar <rajeeshknambiar@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "exmanstorage.h"
#include <QStringList>
#include <QVariant>
#include <QtAlgorithms>

ExManItem::ExManItem()
{
	exclude		= false;
	formulaError	= false;
	/*amount = 0.00;*/
};

ExManItem::ExManItem(QStringList &tokens)
{
	//Sanity check on tokens.count() omitted...
	item		= tokens[0];
	formula		= tokens[1];
	exclude		= (tokens.count() == 3);
	formulaError	= false;
	try {
		float tmpAmt = amount();
	}
	catch (CalculatorException e) {
		formulaError = true;
	}
};

//Initialize the static/singleton member
Calculator *ExManItem::evaluator = new Calculator;

float ExManItem::amount()
{
	if (formula.isEmpty())
		return 0.00;

	//if formula is not empty, pass it to the Calculator to evaluate
	return evaluator->eval(formula.toStdString());
}

ExManEntity::ExManEntity(QString &oneLine)
{
	date = QDate::fromString(oneLine, "dd/MM/yyyy");
};

ExManStorage::ExManStorage(QString &fname):accountingFile(fname)
{
	QString		readLine;
	QStringList	tokens;
	ExManEntity	*newEntity	= NULL;
	ExManItem	*newItem	= NULL;
	if (accountingFile.open(QFile::ReadOnly | QFile::Text)) {
		//Use QTextStream to handle Unicode characters
		QTextStream contents(&accountingFile);
		do {
			readLine = contents.readLine();
			if (readLine.isEmpty())
				continue;

			//Now that a line is read, split it at whitespaces
			tokens = readLine.split(QRegExp("(\t|\n)"),
						QString::SkipEmptyParts);
			//tokens.removeAll("\n");
			if (tokens.isEmpty())
				continue;
			if (tokens.count() == 1) { //Date
				if (newEntity != NULL
				    && (!newEntity->date.isNull())) {
					//Found a second Date entry, so write
					//the previous AccountEntity and clear
					accountingBuffer.append(*newEntity);
					delete newEntity;
				}
				newEntity = new ExManEntity(tokens[0]);
			}
			else {	//Account Items
				newItem = new ExManItem(tokens);
				newEntity->items.append(*newItem);
				delete newItem;
			}
		} while ( ! contents.atEnd() );
		//Add the last entry, which will miss the case 1 inside loop
		if (newEntity != NULL) {
			accountingBuffer.append(*newEntity);
			delete newEntity;
		}
	}
}

ExManStorage::~ExManStorage()
{
	//Close the file if still open
	if (accountingFile.isOpen())
		accountingFile.close();
	//Free the buffer space
	if (accountingBuffer.count()) {
		while (!accountingBuffer.isEmpty())
			accountingBuffer.removeFirst();
	}
}

int ExManStorage::searchWithDate(QDate onDate)
{
	//Returns -1 on failure, otherwise the index of entry found

	//NOTE: we can't use BinarySearch here, as data written while
	//      the user uses the application may not be in the order
	//      of the date. Or, we should sort the accountingBuffer
	//      everytime a new accountingEntity is added - and that's
	//      not good for performance reasons.
	//TODO: probably we should "insert in order", to avoid re-sorting
	ExManEntity searchKey;
	searchKey.date = QDate(onDate);
	return accountingBuffer.indexOf(searchKey);
}

QList <QDate> ExManStorage::getDateList(int year, int month)
{
	QList <QDate> dateList;
	//If year is given, return date in that year only.
	//If month is given, date return will be in that month only.
	foreach(ExManEntity entry, accountingBuffer) {
		if (year && (entry.date.year() != year))
			continue;
		if (month && (entry.date.month() != month))
			continue;
		dateList.append(entry.date);
	}
	return dateList;
}

float ExManStorage::getTotalforDate(QDate onDate, bool withExcluded)
{
	float total = 0.00;
	foreach(ExManItem itm, readData(onDate).items) {
		//withExcluded? total += itm.amount : (itm.exclude? total += itm.amount : total+=0;);
		if (withExcluded && (!itm.formulaError))
			total += itm.amount();
		else if (itm.exclude && (!itm.formulaError))
			total += itm.amount();
	}
	return total;
}

ExManEntity ExManStorage::readData(QDate onDate)
{
	ExManEntity result;
	int index = searchWithDate(onDate);
	if (index != -1) {
		result.date = accountingBuffer[index].date;
		result.items = accountingBuffer[index].items;
	}
	return result;
}

bool ExManStorage::writeData(ExManEntity & data)
{
	if (data.items.isEmpty())
		//All entries for this date is deleted.
		//We need to delete this entry alltogether
		return accountingBuffer.removeAll(data);

	int index = searchWithDate(data.date);
	if (index != -1) {
		accountingBuffer[index] = data;
		return true;
	} else {
		accountingBuffer.append(data);
		return true;
	}
	//TODO: Read back the data written, and return the status
	return false;
}

bool ExManStorage::storeBackend()
{
	//Create a backup
	QString backup = accountingFile.fileName() + ".bak";
	accountingFile.copy(backup);
	//File has been open for reading - close it
	accountingFile.close();
	if (!accountingFile.open(QFile::WriteOnly | QFile::Text))
		return false;
	//Sort entries based on Date before storing into disk
	//qSort needs accountingBuffer to overload "operator <"
	qSort(accountingBuffer.begin(), accountingBuffer.end());
	QTextStream outStream(&accountingFile);
	foreach(ExManEntity entry, accountingBuffer) {
		outStream << "\n" << entry.date.toString("dd/MM/yyyy");
		foreach(ExManItem itm, entry.items) {
			outStream << "\n\t" << itm.item << "\t" << itm.formula;
			if (itm.exclude)
				outStream << "\t" << "X";
		}
	}
	accountingFile.close();
	return true;
}
