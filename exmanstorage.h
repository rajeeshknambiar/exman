/*
 *      exmanstorage.h
 *      
 *      Copyright 2009 Rajeesh K Nambiar <rajeeshknambiar@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#ifndef EXMANSTORAGE_H
#define EXMANSTORAGE_H

#include <QFile>
#include <QTextStream>
#include <QDate>

#include "Calculator.h"

class ExManItem
{
 public:
	QString item;
	QString formula;	//Arithmetical Expression (spreadsheet forumula)
	bool	exclude;	//Exclude this from total or not
	bool	formulaError;	//Erroneous expression or not

	ExManItem ();
	ExManItem (QStringList &tokens);
	float amount();	//Evaluates formula

 protected:
	//Make the calculator singleton
	static Calculator *evaluator;
};

class ExManEntity
{
 public:
	QDate date;
	QList <ExManItem> items;

	ExManEntity () {};
	ExManEntity (QString &oneLine);
	bool operator == (const ExManEntity & other) const
	{ return date == other.date; }
	bool operator < (const ExManEntity & other) const
	{ return date < other.date; }
};

class ExManStorage
{
	//Q_OBJECT

 public:
	ExManStorage(QString & fileName);
	~ExManStorage();

	ExManEntity readData(QDate onDate);
	bool writeData(ExManEntity & data);
	int searchWithDate(QDate onDate);
	QList <QDate> getDateList(int year = 0, int month = 0);
	float getTotalforDate(QDate onDate, bool withExcluded);
	bool storeBackend();

 private:
	 QFile accountingFile;
	 QList <ExManEntity> accountingBuffer;
};

#endif				// EXMANSTORAGE_H
